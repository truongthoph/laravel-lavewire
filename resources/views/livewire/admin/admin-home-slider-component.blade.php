<div>
    <div class="container"  style="padding: 30px 0">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                All Slider
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.addhomeslider') }}" class="btn btn-success pull-right">Add New Product</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('message') }}
                        </div>
                        @endif
                        <table class="table table-strped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Subtitle</th>
                                    <th>Price</th>
                                    <th>Link</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($HomSlider as $hsler)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td><img src="{{asset('assets/images/sliders')}}/{{$hsler->image}}"  width="50"> </td>
                                    <td>{{$hsler->title}}</td>
                                    <td>{{$hsler->subtitle}}</td>
                                    <td>{{$hsler->price}}</td>
                                    <td>{{$hsler->link}}</td>
                                    <td>{{$hsler->status == 1 ? 'Actice':'Inactive'}}</td>
                                    <td>{{$hsler->created_at}}</td>
                                    <td>
                                        <a href="{{ route('admin.edithomeslider',['slider_id'=>$hsler->id]) }}"> <i class="fa fa-edit fa-2x"></i></a>
                                        <a href="#" onclick="confirm('Are you sure, You want to delete this category?') || event.stopImmediatePropagation()"  wire:click.prevent="deleteslider({{$hsler->id}})" style="margin-left: 5px"> <i class="fa fa-times fa-2x text-danger"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$HomSlider->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
