<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\HomSlider;
use Livewire\WithPagination;


class AdminHomeSliderComponent extends Component
{
    use WithPagination;
    public function deleteslider($id)
    {
       $HomSlider = HomSlider::find($id);
       $HomSlider->delete();
       session()->flash('message', 'Deleted successfully');
    }
    public function render()
    {
        $HomSlider = HomSlider::paginate(10);
        return view('livewire.admin.admin-home-slider-component',['HomSlider'=>$HomSlider])->layout('layouts.base');
    }
}
