<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\HomSlider;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class AdminEditHomeSliderComponent extends Component
{
    use WithFileUploads;
    public $title;
    public $subtitle;
    public $price;
    public $link;
    public $status;
    public $image;
    public $newimage;
    public $slider_id;

    public function mount($slider_id)
    {
        $slider =  HomSlider::find($slider_id);
        $this->title    = $slider->title;
        $this->subtitle = $slider->subtitle;
        $this->price    = $slider->price;
        $this->link     = $slider->link;
        $this->status   = $slider->status;
        $this->image    = $slider->image;
        $this->slider_id = $slider->id;
    }

    public function updatehomeslider()
    {
        $homeslider = HomSlider::find($this->slider_id);
        $homeslider->title      = $this->title;
        $homeslider->subtitle   = $this->subtitle;
        $homeslider->price      = $this->price;
        $homeslider->link       = $this->link;
        $homeslider->status     = $this->status;

        if($this->newimage)
        {
            $imageName = carbon::now()->timestamp.'.' .$this->newimage->extension();
            $this->newimage->storeAs('sliders',$imageName);
            $homeslider->image = $imageName;
        }

        $homeslider->save();
        session()->flash('message', 'Ok Saved Slider');
    }

    public function render()
    {
        return view('livewire.admin.admin-edit-home-slider-component')->layout('layouts.base');
    }
}
