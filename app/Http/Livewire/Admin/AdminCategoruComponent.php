<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Category;
use Livewire\WithPagination;

class AdminCategoruComponent extends Component
{
    use WithPagination;

    public function deleteCategory($id)
    {
       $category = Category::find($id);
       $category->delete();
       session()->flash('message', 'delete successfully');
    }
    public function render()
    {
        $categories = Category::paginate(5);

        return view('livewire.admin.admin-categoru-component', ['categories'=>$categories])->layout('layouts.base');
    }
}
