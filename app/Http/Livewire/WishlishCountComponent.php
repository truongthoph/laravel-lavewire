<?php

namespace App\Http\Livewire;

use Livewire\Component;

class WishlishCountComponent extends Component
{
    protected $listeners = ['refreshCompomemt'=>'$refresh'];

    public function render()
    {
        return view('livewire.wishlish-count-component');
    }
}
