<div>
    <div class="container" style="padding: 30px 0">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Add New Product
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('admin.products')}}" class="btn btn-success pull-right">All Products</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">

                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <form  class="form-horizontal" wire:submit.prevent="addProduct" emctype="multipart/form-date">
                            <div class="form-group">
                                <label class="col-md-4 control-label"> Product Name</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Product Name" wire:model="name" wire:keyup="generateslug" class="form-control input-md">
                                    @error('name') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"> Product Slug</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Product Slug"  wire:model="slug" class="form-control input-md">
                                    @error('slug') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"> Short description</label>
                                <div class="col-md-4" wire:ignore>
                                    <textarea class="form-control" id="short_description" placeholder="Short description" wire:model="short_description"></textarea>
                                    @error('short_description') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"> Description</label>
                                <div class="col-md-4" wire:ignore>
                                    <textarea class="form-control" id="description" placeholder="description" wire:model="description"></textarea>
                                    @error('description') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"> Regular Price</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Regular Price"  wire:model="regular_price" class="form-control input-md">
                                    @error('regular_price') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"> Sale Price</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Sale Price"  wire:model="sale_price" class="form-control input-md">
                                    {{-- @error('sale_price') <p class="text-danger">{{$message}}</p> @enderror --}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">SKU</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="SKU"  wire:model="SKU" class="form-control input-md">
                                    @error('SKU') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" >Stock</label>
                                <div class="col-md-4">
                                    <select class="form-control" wire:model="stock_status">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"> Quantity</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Quantity"  wire:model="quantity" class="form-control input-md">
                                    @error('quantity') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"> Product Image</label>
                                <div class="col-md-4">
                                    <input type="file"  wire:model="image" class="input-filr">
                                    @if($image)
                                    <img src="{{$image->temporaryUrl()}}" width="120" />
                                    @endif
                                    @error('image') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Featuref</label>
                                <div class="col-md-4">
                                    <select class="form-control" wire:model="featured">
                                        <option value="instock">Instock</option>
                                        <option value="outofstock">Out Of Stock</option>
                                    </select>
                                    @error('featured') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Category</label>
                                <div class="col-md-4">
                                    <select class="form-control" wire:model="category_id">
                                        @foreach($categories as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                   <button style="submit" class="btn btn-submit">submit </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function(){
        tinymce.init({
        selector: '#short_description',
        setup:function(editor){
            editor.on('Change', function(e){
                tinyMCE.triggerSave();
                var sd_data = $('#short_description').val();
                @this.set('short_description',sd_data);
            });
            }
        });
        tinymce.init({
        selector: '#description',
        setup:function(editor){
            editor.on('Change', function(e){
                tinyMCE.triggerSave();
                var d_data = $('#description').val();
                @this.set('description',d_data);
            });
            }
        });
    });
</script>
@endpush
