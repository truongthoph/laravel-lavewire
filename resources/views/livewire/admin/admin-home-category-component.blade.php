<div>
    <div class="container"  style="padding: 30px 0">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Manage Home Categories
                            </div>
                            {{-- <div class="col-md-6">
                                <a href="{{ route('admin.addhomeslider') }}" class="btn btn-success">Add New Product</a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('message') }}
                        </div>
                        @endif
                         <form class="form-horizontal" wire:submit.prevent="updateHomeCategory">
                             <div class="form-group">
                                 <label class="col-md-4 control-label">Choose Categories</label>
                                <div class="col-md-4" wire:ignore>
                                    <select name="selected_categories[]" multiple="multiple" wire:model="selected_categories" class="sel_categories form-control">
                                        @foreach ($categories as $catrgory)
                                        <option value="{{$catrgory->id}}"> {{$catrgory->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                             </div>
                             <div class="form-group">
                                <label class="col-md-4 control-label">No Of Product</label>
                                <div class="col-md-4">
                                    <input class="form-control input-md"  wire:model="numberofproduct"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button style="submit" class="btn btn-primary" >Save</button>
                                </div>
                            </div>
                         </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(document).ready(function() {
        $('.sel_categories').select2();
        $('.sel_categories').on('change', function(e){
            var data = $('.sel_categories').select2("val");
            @this.set('selected_categories',data);
        });
    });
</script>

@endpush
