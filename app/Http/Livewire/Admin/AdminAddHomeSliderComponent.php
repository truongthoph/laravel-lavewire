<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\HomSlider;
use Livewire\WithFileUploads;
use Carbon\Carbon;

class AdminAddHomeSliderComponent extends Component
{
    use WithFileUploads;
    public $title;
    public $subtitle;
    public $price;
    public $link;
    public $status;
    public $image;

    public function mount()
    {
        $this->status  = 0;
    }
    public function addhomeslider()
    {
        $homeslider = new HomSlider();
        $homeslider->title      = $this->title;
        $homeslider->subtitle   = $this->subtitle;
        $homeslider->price      = $this->price;
        $homeslider->link       = $this->link;
        $homeslider->status     = $this->status;
        $imageName = carbon::now()->timestamp.'.' .$this->image->extension();
        $this->image->storeAs('sliders',$imageName);
        $homeslider->image = $imageName;
        $homeslider->save();
        session()->flash('message', 'Ok Saved Product');
    }

    public function render()
    {
        return view('livewire.admin.admin-add-home-slider-component')->layout('layouts.base');
    }
}
