<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomSlider extends Model
{
    use HasFactory;
    protected $table = "hom_sliders";
}
