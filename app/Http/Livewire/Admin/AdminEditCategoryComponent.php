<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\Category;

class AdminEditCategoryComponent extends Component
{
    public $category_slug;
    public $category_id;
    public $name;
    public $slug;

    public function mount($category_slug)
    {
        $this->category_slug = $category_slug;
        $category = Category::where('slug', $category_slug)->first();
        $this->slug = $category->slug;
        $this->name = $category->name;
        $this->category_id = $category->id;
    }
    public function generateslug()
    {
        $this->slug = Str::slug($this->name);
    }
    public function updated($fileds)
    {
        $this->validateOnly($fileds, [
            'name'=> 'required',
            'slug'=> 'required|unique:categories'
        ]);
    }
    public function updateCategory(){
        $this->validate([
            'name'=> 'required',
            'slug'=> 'required|unique:categories'
       ]);

        $category = Category::find($this->category_id);

        // dd($category);
        $category->name = $this->name;
        $category->slug = $this->slug;
        $category->save();
        session()->flash('message', 'Category has been updated successsfully!');
        // $this->slug = "";
        // $this->name = "";
    }

    public function render()
    {
        return view('livewire.admin.admin-edit-category-component')->layout('layouts.base');
    }
}
